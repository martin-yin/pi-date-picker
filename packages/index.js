import DatePicker from './date-picker';

const install = function(Vue) {
    if (install.installed) return
    Vue.component(DatePicker.name, DatePicker);
}

if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue)
}
export default {
    install,
};

export {
    DatePicker,
};

